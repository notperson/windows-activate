package activate;

import java.util.Properties;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import activate.ui.Win10DigitalLicenseFrame;
import activate.ui.Win7OemActivateFrame;

public class Application {

	public static void main(String[] args) throws ClassNotFoundException, InstantiationException,
			IllegalAccessException, UnsupportedLookAndFeelException {

		UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		Properties props = System.getProperties();
		SwingUtilities.invokeLater(() -> {
			switch (props.getProperty("os.name")) {
			case "Windows 11":
				Win10DigitalLicenseFrame.getIstance();
				Win10DigitalLicenseFrame.getIstance().setWinType("win10_1");
				break;
			case "Windows 10":
				Win10DigitalLicenseFrame.getIstance();
				Win10DigitalLicenseFrame.getIstance().setWinType("win10");
				break;
			case "Windows 7":
				Win7OemActivateFrame.getIstance();
				break;
			default:
				JOptionPane.showMessageDialog(null, "您的系统不支持");
				break;
			}
		});

	}

}
