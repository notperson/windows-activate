package activate.common;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class Static {

	public final static String VERSION = "1.0.0";

	public final static String SYS_TEMP_FOLDER = System.getProperty("java.io.tmpdir");

	public final static LinkedHashMap<String, String> WIN7_OEM_LIC = new LinkedHashMap<String, String>() {
		private static final long serialVersionUID = -1218453951650891267L;
		{
			put("����", "lenovo");
			put("����", "dell");
			put("�곞", "acer");
			put("��˶", "asus");
			put("����", "samsung");
			put("����", "hp");
		}
	};

	public final static LinkedHashMap<String, String> WIN7_OEM_KEY = new LinkedHashMap<String, String>() {
		private static final long serialVersionUID = -1218453951650891267L;
		{
			/* Lenovo */
			put("�����콢��", "22TKD-F8XX6-YG69F-9M66D-PMJBM");
			put("����רҵ��", "237XB-GDJ7B-MV8MH-98QJM-24367");
			put("�����ͥ�߼���", "27GBM-Y4QQC-JKHXW-D9W83-FJQKD");
			/* Dell */
			put("�����콢��", "342DG-6YJR8-X92GV-V7DCV-P4K27");
			put("����רҵ��", "32KD2-K9CTF-M3DJT-4J3WC-733WD");
			put("������ͥ�߼���", "6RBBT-F8VPQ-QCPVQ-KHRB8-RMV82");
			/* Acer */
			put("�곞�콢��", "FJGCP-4DFJD-GJY49-VJBQ7-HYRR2");
			put("�곞רҵ��", "YKHFT-KW986-GK4PY-FDWYH-7TP9F");
			put("�곞��ͥ�߼���", "VQB3X-Q3KP8-WJ2H8-R6B6D-7QJB7");
			/* Asus */
			put("��˶�콢��", "2Y4WT-DHTBF-Q6MMK-KYK6X-VKM6G");
			put("��˶רҵ��", "2WCJK-R8B4Y-CWRF2-TRJKB-PV9HW");
			put("��˶��ͥ�߼���", "2QDBX-9T8HR-2QWT6-HCQXJ-9YQTR");
			/* Samsung */
			put("�����콢��", "49PB6-6BJ6Y-KHGCQ-7DDY6-TF7CD");
			put("����רҵ��", "GMJQF-JC7VC-76HMH-M4RKY-V4HX6");
			put("���Ǽ�ͥ�߼���", "CQBVJ-9J697-PWB9R-4K7W4-2BT4J");
		}
	};

	public final static HashMap<String, String> WIN7_SYS_VERSION = new HashMap<String, String>() {
		private static final long serialVersionUID = -8123424051213068598L;
		{
			put("Ultimate", "�콢��");
			put("Professional", "רҵ��");
			put("HomePremium", "��ͥ�߼���");
		}
	};

	public final static HashMap<String, String> WIN10_DIGITAL_LICENSE_SKU = new HashMap<String, String>() {
		private static final long serialVersionUID = 5467201908687816383L;
		{
			put("Cloud", "178");
			put("CloudN", "179");
			put("Core", "101");
			put("CoreN", "98");
			put("CoreCountrySpecific", "99");
			put("CoreSingleLanguage", "100");
			put("Education", "121");
			put("EducationN", "122");
			put("Enterprise", "4");
			put("EnterpriseN", "27");
			put("EnterpriseS", "125");
			put("EnterpriseSN", "126");
			put("Professional", "48");
			put("ProfessionalN", "49");
			put("ProfessionalEducation", "164");
			put("ProfessionalEducationN", "165");
			put("ProfessionalWorkstation", "161");
			put("ProfessionalWorkstationN", "162");
		}
	};

	public final static HashMap<String, String> WIN10_DIGITAL_LICENSE_KEY = new HashMap<String, String>() {
		private static final long serialVersionUID = -8123424051213068598L;
		{
			/* Cloud */ put("178", "V3WVW-N2PV2-CGWC3-34QGF-VMJ2C");
			/* CloudN */ // put("179", "");
			/* Core */ put("101", "YTMG3-N6DKC-DKB77-7M9GH-8HVX7");
			/* CoreN */ // put("98", "");
			/* CoreCountrySpecific */ put("99", "N2434-X9D7W-8PF6X-8DV9T-8TYMD");
			/* CoreSingleLanguage */ put("100", "BT79Q-G7N6G-PGBYW-4YWX6-6F4BT");
			/* Professional */ put("48", "VK7JG-NPHTM-C97JM-9MPGT-3V66T");
			/* Education */ put("121", "YNMGQ-8RYV3-4PGQ3-C8XTP-7CFBY");
			/* Enterprise */ put("4", "XGVPP-NMH47-7TTHJ-W3FW7-8HV2C");
			/* EnterpriseS */ put("125", "NK96Y-D9CD8-W44CQ-R8YTK-DYJWX");
			/* ProfessionalEducation */ put("164", "8PTT6-RNW4C-6V7J2-C2D3X-MHBPB");
			/* ProfessionalWorkstation */ put("161", "DXG7C-N36C4-C4HTG-X4T3X-2YV77");
		}
	};

}
