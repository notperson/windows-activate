package run;

import java.util.Properties;

import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import ui.Win10DigitalLicenseJPanel;
import ui.Win7OemActivatieJPanel;

public class Main {

	public static void main(String[] args) throws ClassNotFoundException, InstantiationException,
			IllegalAccessException, UnsupportedLookAndFeelException {

		UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		Properties props = System.getProperties();
		switch (props.getProperty("os.name")) {
		case "Windows 10":
			Win10DigitalLicenseJPanel.getIstance();
			break;
		case "Windows 7":
			Win7OemActivatieJPanel.getIstance();
			break;
		default:
			JOptionPane.showMessageDialog(null, "您的系统不支持");
			break;
		}

	}

}
